const axios = require('axios');

let temperatura = {};

const getClima = async(lat, lng) => {
    let resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${ lat }&lon=${ lng }&lang=es&units=metric&appid=85e0c0307b5563c14ca784a6acececcb`);

    if (typeof resp.data.main !== 'undefined') {
        temperatura.current = resp.data.main.temp;
        temperatura.min = resp.data.main.temp_min;
        temperatura.max = resp.data.main.temp_max;
    } else {
        temperatura = false;
    }

    return temperatura;
}

module.exports = {
    getClima
}
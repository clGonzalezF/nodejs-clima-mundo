const axios = require('axios');

let location = {};

const getLugarLatLng = async(direccion) => {
    let encodeUrl = encodeURI(direccion);
    let resp = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${ encodeUrl }&key=AIzaSyA-HXVa2jtkGfKtIJwisxgC46RaWqC1xuI`);

    if (resp.data.status == 'ZERO_RESULTS') {
        //throw new Error('No se ecuentra la ciudad');
        location = false;
    } else {
        location = resp.data.results[0].geometry.location
        location.direccion = resp.data.results[0].formatted_address;
    }

    return location;
}

module.exports = {
    getLugarLatLng
}
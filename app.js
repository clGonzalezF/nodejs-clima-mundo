const argv = require('./config/yargs').argv;
const colors = require('colors');
const { getLugarLatLng } = require('./lugar/lugar');
const { getClima } = require('./clima/clima');

let comando = argv._;

let getClimaDeLugar = async() => {
    let ubicacion = await getLugarLatLng(argv.direccion);
    if (ubicacion != false) {
        let temperatura = await getClima(ubicacion.lat, ubicacion.lng);
        console.log('====== TEMPERATURA ======'.yellow);
        console.log(`Lugar: ${ ubicacion.direccion }`);
        console.log(`TempActual: ${ temperatura.current }`);
        console.log(`TempMin: ${ temperatura.min }`);
        console.log(`TempMax: ${ temperatura.max }`);
        console.log('========================='.yellow);
    } else {
        console.log(`No se pudo obetener el clima del lugar ingresado :: "${ argv.direccion }"`);
    }
};

getClimaDeLugar();